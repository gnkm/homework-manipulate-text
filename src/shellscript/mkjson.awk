BEGIN {
	FS = ","
	prev_project = ""
	prev_user = ""
	print "{"
	print "  'name': 'projects',"
	print "  'data': ["
}
$1 != "" && $2 != "" && $3 != "" {
	project = $1
	user = $2
	type = $3
	time = $4
	if (project != prev_project) {
		if (prev_project != "") {
			print "            }" # time-data
			print "          ]"			# name-data
			print "        }"			# name
			print "      ]"
			print "    },"
		}
		print "    {"
		print "      'project': '" project "',"
		print "      'data': ["
		prev_project = project
		prev_user = ""
	}
	if (user != prev_user) {
		if (prev_user != "") {
			print "            }" # time-data
			print "          ]"	# name-data
			print "        },"
		}
		print "        {"
		print "          'name': '" user "',"
		print "          'data': ["
		prev_user = user
	} else {
			print "            }," # time-data
	}
	print "            {"
	print "              'type': '" type "',"
	print "              'time': " time
	# print "            }"


}
END {
	print "            }"		# time-data
	print "          ]"			# name-data
	print "        }"			# name
	print "      ]"				# project-data
	print "    }"
	print "  ]"
	print "}"
}
