function hm2m(hm) {
	split($7, ht, ":")
	hour = ht[1]
	min = ht[2]
	return 60 * hour + min
}
function exists(needle, haystack) {
	for (i in haystack) {
		if (needle == i) {
			return 1
		}
	}
	return 0
}
BEGIN {
	FS = ","
	projects[""] = ""
	types[""] = ""
	# project, name, type
	data[",,"] = 0
}
$3 != "" && $5 != "" && $6 != "" {
	project = $5
	name = $3
	type = $6
	# projects
	if (!exists(project, projects)) {
		projects[project] = 1
	}
	# time
	data[project "," name "," type] += hm2m($7)
	# debug
	# print data[project, name, type]
}
END {
	for (project_name_type in data) {
		print project_name_type "," data[project_name_type]
	}
}
