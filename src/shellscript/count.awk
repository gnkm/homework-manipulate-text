BEGIN {
	FS = ","
	cnts[""] = 0
}
$2 != "" {
	split($7, ht, ":")
	hour = ht[1]
	min = ht[2]
	cnts[$5] += 60 * hour + min
	# debug
	# print NR " :: " $5 ":" cnts[$5]
}
END {
	for (i in cnts) {
		if (i != "") {
			print i "," cnts[i]
		}
	}
}
